public class RegularUser extends User{
    AccountHolder accH;
    int numOfAcc;

    public RegularUser(){
        super();
        
    }
    public RegularUser(String id, String d, String n){
        super(id, d , n);
        numOfAcc = 0;
        accH = new AccountHolder(id, d, n);
    }

    public void applyAccount(Administrator admin, String accId, double balance, String currency, String type){
        admin.reqOpenAcc(userId, accId, balance, currency, type);
    }

    public void applyAccount(Administrator admin, Account acc, String type){
        admin.reqOpenAcc(userId, acc.accId, acc.balance, acc.currency, type);
    }

    public void closeAcc(Administrator admin, String accId, String currency, String type){
        admin.reqCloseAcc(userId, accId, currency, type);
    }

    public void cancelLastAccApplication(Administrator admin){
        admin.accReq.remove(admin.accReq.size() - 1);
    }

    public void addAccount(String accId, String currency, String type){
        if(numOfAcc == 0){
            AccountOwnership newAcc = new AccountOwnership(super.userId, accId, currency, type);
            accH = new AccountHolder(super.userId, super.name, super.dob, newAcc);
            numOfAcc++;
        }else{
            accH.addOwnership(accId, currency, type);
            numOfAcc++;
        }
    }

    public void remAccount(String accId){
        for(int i = 0; i < numOfAcc; i++){
            if(accId.equals(accH.accOwn.get(i).accId)){
                accH.accOwn.remove(i);
                break;
            }
        }
    }

    public void applyCard(Administrator admin, String accId, String cardId, String expiryDate){
        admin.reqAddCard(userId, accId, cardId, expiryDate);
    }

    public void remCard(Administrator admin, String accId, String cardId){
        admin.reqRemoveCard(userId, accId, cardId);
    }

    public void cancelLastCardReq(Administrator admin){
        admin.cardReq.remove(admin.cardReq.size() - 1);
    }
}
