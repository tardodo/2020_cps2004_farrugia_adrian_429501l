public class AccountRequest extends Request{
    double balance;
    String currency;
    String type;

    public AccountRequest(String userId, String accId, String currency, String type, boolean create){
        super(userId, accId, create);
        this.type = type;
        balance = 0;
        this.currency = currency;
        balance = 0;
    }

    public AccountRequest(String userId, String accId, double balance, String currency, String type, boolean create){
        super(userId, accId, create);
        this.type = type;
        this.balance = balance;
        this.currency = currency;
        balance = 0;
    }
}
