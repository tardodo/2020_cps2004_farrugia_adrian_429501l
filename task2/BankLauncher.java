public class BankLauncher {
    public static void main(String []args){
        BankLauncher bl = new BankLauncher();
        bl.testView();
        bl.testTransaction();
        bl.testAddRemoveInstr();
        bl.testJointAccounts();
        bl.testMultipleBankCards();
    }   

    // View accounts of a user.
    // Output: Display of two accounts of a single user.
    public void testView(){
        System.out.println("--------Test View--------\n");
        SavingsMethods savList = new SavingsMethods();
        CurrentMethods currList = new CurrentMethods();
        RegUserMethods regUserList = new RegUserMethods();
        AdminMethods adminList = new AdminMethods();

        regUserList.init();
        adminList.init();
        currList.init();
        savList.init();
        // Create user
        regUserList.add("1234U", "12/06/2001", "Thomas");
        // Create admin
        adminList.add(new Administrator("6789A", "25/04/1995", "Jones"));
        // Apply for account
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234SA", 50, "Eur", "save");
        // Review account request
        adminList.retrieveUser("6789A").reviewAccReq(regUserList, currList, savList, true);
        // View all accounts of user
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        // Apply for another account
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234CA", 20, "Eur", "current");
        // Review account request
        adminList.retrieveUser("6789A").reviewAccReq(regUserList, currList, savList, true);
        // View all accounts of user
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        System.out.println("-------------------------\n");
    }

    // A user owning two accounts moves money between them.
    // Output: Display user accounts, showing the before and after. There is a transaction log to further show an exchange happened.
    public void testTransaction(){
        System.out.println("--------Test Transactions--------\n");
        SavingsMethods savList = new SavingsMethods();
        CurrentMethods currList = new CurrentMethods();
        RegUserMethods regUserList = new RegUserMethods();
        AdminMethods adminList = new AdminMethods();

        regUserList.init();
        adminList.init();
        currList.init();
        savList.init();
        regUserList.add("1234U", "12/06/2001", "Thomas");
        adminList.add(new Administrator("6789A", "25/04/1995", "Jones"));
        // Apply for 2 accounts. One savings, the other current.
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), new Savings("Eur", 150.00, "1234SA"), "save");
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), new Current("Eur", 50.00, "1234CA"), "current");
        adminList.retrieveUser("6789A").reviewAccReq("1234U" ,regUserList, currList, savList, true);
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        // Transfer money between them
        regUserList.retrieveUser("1234U").accH.transfer("1234SA", "1234CA", 30.00, currList, savList);

        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        System.out.println("-------------------------\n");
    }

    // Two accounts are applied for, as well as a bank card, however the latest account request and the bank card request
    //  are cancelled. 
    // Output: Display of only one account.
    public void testAddRemoveInstr(){
        System.out.println("--------Test Adding/Removing Instructions to Admins--------\n");
        SavingsMethods savList = new SavingsMethods();
        CurrentMethods currList = new CurrentMethods();
        RegUserMethods regUserList = new RegUserMethods();
        AdminMethods adminList = new AdminMethods();

        regUserList.init();
        adminList.init();
        currList.init();
        savList.init();
        regUserList.add("1234U", "12/06/2001", "Thomas");
        adminList.add(new Administrator("6789A", "25/04/1995", "Jones"));
        // Apply for 2 accounts
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234SA", 50, "Eur", "save");
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "4321SA", 100, "DLR", "save");
        // Cancel latest account application
        regUserList.retrieveUser("1234U").cancelLastAccApplication(adminList.retrieveUser("6789A"));
        // Apply for card
        regUserList.retrieveUser("1234U").applyCard(adminList.retrieveUser("6789A"), "1234SA", "1234BC", "21/04/2022");
        // Cancel card request
        regUserList.retrieveUser("1234U").cancelLastCardReq(adminList.retrieveUser("6789A"));
        // Search by user to exhaust all account requests by the user.
        adminList.retrieveUser("6789A").reviewAccReq("1234U", regUserList, currList, savList, true);
        // Review latest card request just in case, however there are none as it's been cancelled.
        adminList.retrieveUser("6789A").reviewCardReq(regUserList, currList, savList, true);
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        System.out.println("-------------------------\n");
    }

    // Users can have individual or joint accounts. Here two users share the same account. A user also has multiple accounts.
    // Output: Display of accounts for both users, showing the individual and joint ones.
    public void testJointAccounts(){
        System.out.println("--------Test Joint Accounts--------\n");
        SavingsMethods savList = new SavingsMethods();
        CurrentMethods currList = new CurrentMethods();
        RegUserMethods regUserList = new RegUserMethods();
        AdminMethods adminList = new AdminMethods();

        regUserList.init();
        adminList.init();
        currList.init();
        savList.init();
        // 2 users
        regUserList.add("1234U", "12/06/2001", "Thomas");
        regUserList.add("4567U", "24/07/2003", "Bob");
        adminList.add(new Administrator("6789A", "25/04/1995", "Jones"));
        // Apply for 2 accounts for user1
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234SA", 50, "EUR", "save");
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234CA", 70, "DLR", "current");
        // Accept all accounts for user1
        adminList.retrieveUser("6789A").reviewAccReq("1234U", regUserList, currList, savList, true);
        // Apply for existing account for user2
        regUserList.retrieveUser("4567U").applyAccount(adminList.retrieveUser("6789A"), savList.retrieveAcc("1234SA"), "save");
        adminList.retrieveUser("6789A").reviewAccReq(regUserList, currList, savList, true);
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        regUserList.retrieveUser("4567U").accH.viewAllAcc(currList, savList);
    
        System.out.println("-------------------------\n");
    }

    // Here, the user applies for 2 accounts and for 2 bank cards on one of the accounts. 
    // Output: Display of user accounts, showing the 2 bank cards on the single account.
    public void testMultipleBankCards(){
        System.out.println("--------Test Multiple Bank Cards with an Account--------\n");
        SavingsMethods savList = new SavingsMethods();
        CurrentMethods currList = new CurrentMethods();
        RegUserMethods regUserList = new RegUserMethods();
        AdminMethods adminList = new AdminMethods();

        regUserList.init();
        adminList.init();
        currList.init();
        savList.init();
        regUserList.add("1234U", "12/06/2001", "Thomas");
        adminList.add(new Administrator("6789A", "25/04/1995", "Jones"));
        // Apply for 2 accounts
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "1234SA", 50, "Eur", "save");
        regUserList.retrieveUser("1234U").applyAccount(adminList.retrieveUser("6789A"), "4321SA", 100, "DLR", "save");
        // Apply for 2 cards on the same account
        regUserList.retrieveUser("1234U").applyCard(adminList.retrieveUser("6789A"), "1234SA", "1234BC", "21/04/2022");
        regUserList.retrieveUser("1234U").applyCard(adminList.retrieveUser("6789A"), "1234SA", "4321BC", "13/08/2023");
        // Search by user to exhaust all account requests by the user.
        adminList.retrieveUser("6789A").reviewAccReq("1234U", regUserList, currList, savList, true);
        // Search by user to exhaust all card requests by the user.
        adminList.retrieveUser("6789A").reviewCardReq("1234U",regUserList, currList, savList, true);
        regUserList.retrieveUser("1234U").accH.viewAllAcc(currList, savList);
        System.out.println("-------------------------\n");
    }
}
