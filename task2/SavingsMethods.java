import java.util.ArrayList;
import java.util.List;

public class SavingsMethods{
    List<Savings> savAcc;

    public void init(){
        savAcc = new ArrayList<>();
    }

    public void add(String c, double b, String a){
        Savings save = new Savings(c, b ,a);
        savAcc.add(save);
    }

    public void remove(String accId){
        savAcc.remove(retrieveAcc(accId));
    }

    public int numOfAcc(){
        return savAcc.size();
    }
    public void display(){
        if(numOfAcc() == 0){
            System.out.println("No savings accounts");
        }else{
            for(int i = 0; i < numOfAcc(); i++){
                System.out.println(savAcc.get(i).accId);
                System.out.println(savAcc.get(i).balance);
                System.out.println(savAcc.get(i).currency);
            }
        }
    }

    public boolean findAcc(String accId){
        for(int i = 0; i < numOfAcc(); i++){
            if(accId.equals(savAcc.get(i).accId)){
                return true;
            }
        }
        return false;
    }

    public Savings retrieveAcc(String accId){
        Savings acc = new Savings();
        for(int i = 0; i < numOfAcc(); i++){
            if(accId.equals(savAcc.get(i).accId)){
                acc = savAcc.get(i);
                break;
            }
        }
        return acc;
    }
}