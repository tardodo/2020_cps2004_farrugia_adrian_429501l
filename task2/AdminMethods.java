import java.util.ArrayList;
import java.util.List;

public class AdminMethods {
    List<Administrator> admins;

    public void init(){
        admins = new ArrayList<>();
    }

    public void add(Administrator admin){
        admins.add(admin);
    }

    public void remove(Administrator admin){
        admins.remove(admin);
    }

    public int numOfUsers(){
        return admins.size();
    }
    public void display(){
        if(numOfUsers() == 0){
            System.out.println("No administrators");
        }else{
            for(int i = 0; i < numOfUsers(); i++){
                System.out.println(admins.get(i));
            }
        }
    }

    public boolean findUser(String userId){
        for(int i = 0; i < numOfUsers(); i++){
            if(userId.equals(admins.get(i).userId)){
                return true;
            }
        }
        return false;
    }

    public Administrator retrieveUser(String userId){
        Administrator user = new Administrator();
        for(int i = 0; i < numOfUsers(); i++){
            if(userId.equals(admins.get(i).userId)){
                user = admins.get(i);
                break;
            }
        }
        return user;
    }
}
