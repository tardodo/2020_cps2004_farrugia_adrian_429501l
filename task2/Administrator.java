import java.util.*;

public class Administrator extends User{
    List<AccountRequest> accReq;
    List<CardRequest> cardReq;
    
    public Administrator(){
        super();
        accReq = new ArrayList<>();
        cardReq = new ArrayList<>();
    }
    public Administrator(String id, String d, String n){
        super(id, d, n);
        accReq = new ArrayList<>();
        cardReq = new ArrayList<>();
    }

    public void reqOpenAcc(String userId, String accId, double balance, String currency, String type){
        accReq.add(new AccountRequest(userId, accId, balance, currency, type, true));
    }

    public void reqCloseAcc(String userId, String accId, String currency, String type){
        accReq.add(new AccountRequest(userId, accId, currency, type, false));
    }


    public void reviewAccReq(RegUserMethods users, CurrentMethods curr, SavingsMethods sav, boolean accept){
        if (accReq.size() > 0) {
            AccountRequest currReq = accReq.get(accReq.size() - 1);
            RegularUser user;

            if (currReq.create) {
                if(accept){
                    user = users.retrieveUser(currReq.userId);
                    user.addAccount(currReq.accId, currReq.currency, currReq.type);
                    if (currReq.type.equals("save")) {
                        if(!sav.findAcc(currReq.accId))
                            sav.add(currReq.currency, currReq.balance, currReq.accId);
                        sav.retrieveAcc(currReq.accId).addOwnership(currReq.userId, currReq.type);
                    } else {
                        if(!curr.findAcc(currReq.accId))
                            curr.add(new Current(currReq.currency, currReq.balance, currReq.accId));
                        curr.retrieveAcc(currReq.accId).addOwnership(currReq.userId, currReq.type);
                    }
                    System.out.println("Account created");
                    accReq.remove(accReq.size() - 1);
                } else {
                    System.out.println("Account not approved");
                }
            } else {
                user = users.retrieveUser(currReq.userId);
                user.remAccount(currReq.accId);
                if (currReq.type.equals("save")) {
                    sav.remove(currReq.accId);
                } else {
                    curr.remove(currReq.accId);
                }
                System.out.println("Account removed");
                accReq.remove(accReq.size() - 1);
            }
        }else{
            System.out.println("No Account Requests");
        }
    }

    public void reviewAccReq(String userId, RegUserMethods users, CurrentMethods curr, SavingsMethods sav, boolean accept){
        AccountRequest currReq;
        RegularUser user;

        for(int i = 0; i < accReq.size(); i++){
            currReq = accReq.get(i);
            if(currReq.userId.equals(userId)){
                if(currReq.create){
                    if(accept){
                        user = users.retrieveUser(userId);
                        user.addAccount(currReq.accId, currReq.currency, currReq.type);
                        if(currReq.type.equals("save")){
                            if(!sav.findAcc(currReq.accId))
                                sav.add(currReq.currency, currReq.balance, currReq.accId);
                            sav.retrieveAcc(currReq.accId).addOwnership(currReq.userId, currReq.type);
                        }else{
                            if(!curr.findAcc(currReq.accId))
                                curr.add(new Current(currReq.currency, currReq.balance, currReq.accId));
                            curr.retrieveAcc(currReq.accId).addOwnership(currReq.userId, currReq.type);
                        }
                        System.out.println("Account created");
                        accReq.remove(i);
                        i--;
                    }else{
                        System.out.println("Account not approved");
                    }
                }else{
                    user = users.retrieveUser(userId);
                    user.remAccount(currReq.accId);
                    if(currReq.type.equals("save")){
                        sav.remove(currReq.accId);
                    }else{
                        curr.remove(currReq.accId);
                    }
                    System.out.println("Account removed");
                    accReq.remove(i);
                    i--;
                }
            }
        }
    }

    public void reqAddCard(String userId, String accId, String cardId, String expiryDate){
        cardReq.add(new CardRequest(userId, accId, cardId, expiryDate, true));
    }

    public void reqRemoveCard(String userId, String accId, String cardId){
        cardReq.add(new CardRequest(userId, accId, cardId, "", false));
    }

    public void reviewCardReq(RegUserMethods users, CurrentMethods curr, SavingsMethods sav, boolean accept){
        if (cardReq.size() > 0) {
            CardRequest currReq = cardReq.get(cardReq.size() - 1);
            RegularUser user;

            if (currReq.create) {
                if(accept){
                    user = users.retrieveUser(currReq.userId);
                    if (sav.findAcc(currReq.accId)) {
                        sav.retrieveAcc(currReq.accId).addCard(currReq.cardId, currReq.userId, user.name,
                                currReq.expiryDate);
                        System.out.println("Card created");
                    } else if (curr.findAcc(currReq.accId)) {
                        curr.retrieveAcc(currReq.accId).addCard(currReq.cardId, currReq.userId, user.name,
                                currReq.expiryDate);
                        System.out.println("Card created");
                    } else {
                        System.out.println("Account doesn't exist");
                    }

                    cardReq.remove(cardReq.size() - 1);
                } else {
                    System.out.println("Card not approved");
                }
            } else {
                user = users.retrieveUser(currReq.userId);
                if (sav.findAcc(currReq.accId)) {
                    sav.retrieveAcc(currReq.accId).remCard(currReq.cardId);
                    System.out.println("Card removed");
                } else if (curr.findAcc(currReq.accId)) {
                    curr.retrieveAcc(currReq.accId).remCard(currReq.cardId);
                    System.out.println("Card removed");
                } else {
                    System.out.println("Account doesn't exist");
                }
                cardReq.remove(cardReq.size() - 1);
            }
        }else{
            System.out.println("No Card Requests");
        }
    }

    public void reviewCardReq(String userId, RegUserMethods users, CurrentMethods curr, SavingsMethods sav, boolean accept){
        CardRequest currReq;
        RegularUser user;

        for(int i = 0; i < cardReq.size(); i++){
            currReq = cardReq.get(i);
            if(currReq.userId.equals(userId)){
                if(currReq.create){
                    if(accept){
                        user = users.retrieveUser(currReq.userId);
                        if(sav.findAcc(currReq.accId)){
                            sav.retrieveAcc(currReq.accId).addCard(currReq.cardId, currReq.userId, user.name, currReq.expiryDate);
                            System.out.println("Card created");
                        }else if(curr.findAcc(currReq.accId)){
                            curr.retrieveAcc(currReq.accId).addCard(currReq.cardId, currReq.userId, user.name, currReq.expiryDate);
                            System.out.println("Card created");
                        }else{
                            System.out.println("Account doesn't exist");
                        }
                        cardReq.remove(i);
                        i--;
                    }else{
                        System.out.println("Card not approved");
                    }
                }else{
                    user = users.retrieveUser(currReq.userId);
                    if(sav.findAcc(currReq.accId)){
                        sav.retrieveAcc(currReq.accId).remCard(currReq.cardId);
                        System.out.println("Card removed");
                    }else if(curr.findAcc(currReq.accId)){
                        curr.retrieveAcc(currReq.accId).remCard(currReq.cardId);
                        System.out.println("Card removed");
                    }else{
                        System.out.println("Account doesn't exist");
                    }
                    cardReq.remove(i);
                    i--;
                }
            }
        }
    }
}
