public class BankCard {
    String name;
    String expiryDate;
    String userId;
    String cardId;

    public BankCard(String cId, String n, String eD, String uId){
        cardId = cId;
        name = n;
        expiryDate = eD;
        userId = uId;
    }
}
