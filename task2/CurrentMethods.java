import java.util.ArrayList;
import java.util.List;

public class CurrentMethods {
    List<Current> currAcc;

    public void init(){
        currAcc = new ArrayList<>();
    }

    public void add(Current curr){
        currAcc.add(curr);
    }

    public void remove(String accId){
        currAcc.remove(retrieveAcc(accId));
    }

    public int numOfAcc(){
        return currAcc.size();
    }
    public void display(){
        if(numOfAcc() == 0){
            System.out.println("No current accounts");
        }else{
            for(int i = 0; i < numOfAcc(); i++){
                System.out.println(currAcc.get(i).accId);
                System.out.println(currAcc.get(i).balance);
                System.out.println(currAcc.get(i).currency);
            }
        }
    }

    public boolean findAcc(String accId){
        for(int i = 0; i < numOfAcc(); i++){
            if(accId.equals(currAcc.get(i).accId)){
                return true;
            }
        }
        return false;
    }

    public Current retrieveAcc(String accId){
        Current acc = new Current();
        for(int i = 0; i < numOfAcc(); i++){
            if(accId.equals(currAcc.get(i).accId)){
                acc = currAcc.get(i);
                break;
            }
        }
        return acc;
    }
}
