import java.util.ArrayList;
import java.util.List;

public class AccountHolder {
    String personId;
    String name;
    String dob;
    List<AccountOwnership> accOwn;

    public AccountHolder(){
        personId = "";
        name = "";
        dob = "";
        accOwn = new ArrayList<>();
    }
    public AccountHolder(String id, String n, String d){
        personId = id;
        name = n;
        dob = d;
        accOwn = new ArrayList<>();
    }
    public AccountHolder(String id, String n, String d, AccountOwnership acc){
        personId = id;
        name = n;
        dob = d;
        accOwn = new ArrayList<>();
        accOwn.add(acc);
    }

    public void addOwnership(String accId, String currency, String type){
        AccountOwnership newAcc = new AccountOwnership(personId, accId, currency, type);
        accOwn.add(newAcc);
    }

    public void viewAcc(String accId, CurrentMethods currAcc, SavingsMethods savAcc){
        Account acc;
        for(int i = 0; i < accOwn.size(); i++){
            if(accId.equals(accOwn.get(i).accId)){
                if(accOwn.get(i).type.equals("save")){
                    acc = savAcc.retrieveAcc(accId);
                    acc.display();
                    acc.addTrans("Viewed Account");
                }else{
                    acc = currAcc.retrieveAcc(accId);
                    acc.display();
                    acc.addTrans("Viewed Account");
                }
                break;
            }
        }
    }

    public void viewAllAcc(CurrentMethods currAcc, SavingsMethods savAcc){
        Account acc;
        for(int i = 0; i < accOwn.size(); i++){
            if(accOwn.get(i).type.equals("save")){
                acc = savAcc.retrieveAcc(accOwn.get(i).accId);
                System.out.println("Account type: Savings");
                acc.display();
                acc.addTrans("Viewed Account");
            }else{
                acc = currAcc.retrieveAcc(accOwn.get(i).accId);
                System.out.println("Account type: Current");
                acc.display();
                acc.addTrans("Viewed Account");
            }
            System.out.println();
        }
    }

    public void transfer(String fromAccId, String toAccId, double amount, CurrentMethods currAcc, SavingsMethods savAcc){
        Account acc;
        boolean allow = false;
        for(int i = 0; i < accOwn.size(); i++){
            if(fromAccId.equals(accOwn.get(i).accId)){
                if(accOwn.get(i).type.equals("save")){
                    acc = savAcc.retrieveAcc(fromAccId);
                    acc.remMoney(amount);
                    acc.addTrans("Removed Money");
                }else{
                    acc = currAcc.retrieveAcc(fromAccId);
                    acc.remMoney(amount);
                    acc.addTrans("Removed Money");
                }
                allow = true;
                break;
            }
        }

        if(allow){
            allow = false;
            for(int i = 0; i < accOwn.size(); i++){
                if(toAccId.equals(accOwn.get(i).accId)){
                    if(accOwn.get(i).type.equals("save")){
                        acc = savAcc.retrieveAcc(toAccId);
                        acc.addMoney(amount);;
                        acc.addTrans("Added Money");
                    }else{
                        acc = currAcc.retrieveAcc(toAccId);
                        acc.addMoney(amount);
                        acc.addTrans("Added Money");
                    }
                    allow = true;
                    break;
                }
            }
        }

        if(allow == false)System.out.println("Invalid accounts");
    }
    
}
