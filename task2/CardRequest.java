public class CardRequest extends Request{
    String cardId;
    String expiryDate;

    public CardRequest(String userId, String accId, String cardId, String expiryDate, boolean create){
        super(userId, accId, create);
        this.cardId = cardId;
        this.expiryDate = expiryDate;
    }
}
