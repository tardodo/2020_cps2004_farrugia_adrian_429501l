import java.util.ArrayList;
import java.util.List;

public class RegUserMethods {
    List<RegularUser> regUsers;

    public void init(){
        regUsers = new ArrayList<>();
    }

    public void add(String id, String d, String n){
        RegularUser rUser = new RegularUser(id, d, n);
        regUsers.add(rUser);
    }

    public void remove(String id){
        regUsers.remove(retrieveUser(id));
    }

    public int numOfUsers(){
        return regUsers.size();
    }
    public void display(){
        if(numOfUsers() == 0){
            System.out.println("No regular users");
        }else{
            for(int i = 0; i < numOfUsers(); i++){
                System.out.println(regUsers.get(i).userId);
                System.out.println(regUsers.get(i).name);
                System.out.println(regUsers.get(i).dob);
                
            }
        }
    }

    public boolean findUser(String userId){
        for(int i = 0; i < numOfUsers(); i++){
            if(userId.equals(regUsers.get(i).userId)){
                return true;
            }
        }
        return false;
    }

    public RegularUser retrieveUser(String userId){
        RegularUser user = new RegularUser();
        for(int i = 0; i < numOfUsers(); i++){
            if(userId.equals(regUsers.get(i).userId)){
                user = regUsers.get(i);
                break;
            }
        }
        return user;
    }
}
