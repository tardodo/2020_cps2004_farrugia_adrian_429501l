import java.util.ArrayList;
import java.util.List;

public class Account{
    String currency;
    double balance;
    List<String> transactions;
    String accId;
    List<AccountOwnership> accOwn;
    List<BankCard> cards;

    public Account(){
        currency = "";
        balance = 0;
        transactions = new ArrayList<>();
        accId = "";
        accOwn = new ArrayList<>();
        cards = new ArrayList<>();
    }
    public Account(String c, double b, String a){
        currency = c;
        balance = b;
        accId = a;
        transactions = new ArrayList<>();
        accOwn = new ArrayList<>();
        cards = new ArrayList<>();
    }
    
    public void addOwnership(String personId, String type){
        AccountOwnership newAcc = new AccountOwnership(personId, accId, currency, type);
        accOwn.add(newAcc);
    }

    public void display(){
        System.out.println("Account ID: "+accId);
        System.out.println("Currency: "+currency);
        System.out.println("Balance: "+balance);
        System.out.println("Bank Cards: ");
        for(int i = 0; i < cards.size(); i++){
            System.out.println("\t"+ cards.get(i).cardId);
        }
        System.out.println("Account Owners: ");
        for(int i = 0; i < accOwn.size(); i++){
            System.out.println("\t"+ accOwn.get(i).personId);
        }
        System.out.println("Transactions: ");
        for(int i = 0; i < transactions.size(); i++){
            System.out.println("\t"+ transactions.get(i));
        }
    }

    public void addTrans(String trans){
        transactions.add(trans);
    }

    public void remTrans(String trans){
        transactions.remove(trans);
    }

    public void remMoney(double amount){
        if(amount < balance){
            balance = balance - amount;
        }else{
            System.out.println("Not sufficient balance");
        }

    }

    public void addMoney(double amount){
        balance = balance + amount;
    }

    public void addCard(String cardId, String userId, String name, String expiryDate){
        cards.add(new BankCard(cardId, name, expiryDate, userId));
    }

    public void remCard(String cardId){
        for(int i = 0; i < cards.size();i++){
            if(cardId.equals(cards.get(i).cardId)){
                cards.remove(i);
            }
        }
    }
}