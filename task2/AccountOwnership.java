

public class AccountOwnership {
    String personId;
    String accId;
    String currency;
    String type;

    public AccountOwnership(String personId, String accId, String currency, String type){
        this.personId = personId;
        this.accId = accId;
        this.currency = currency;
        this.type = type;
    }
}
