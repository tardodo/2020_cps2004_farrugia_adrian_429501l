public class User {
    String userId;
    String dob;
    String name;

    public User(){
        userId = "";
        dob = "";
        name = "";
    }

    public User(String id, String d, String n){
        userId = id;
        dob = d;
        name = n;
    }
}
