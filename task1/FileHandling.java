import java.io.*;
import java.util.*;

public class FileHandling{
    private String fileName;
    private String splitName[];
    private boolean csv;
    private boolean txt;

    public FileHandling(String name){
        splitName = name.split("\\.");  
        fileName = name;
        try{
            if(splitName[1].equals("csv")){
                csv = true;
                txt = false;
            }else if(splitName[1].equals("txt")){
                txt = true;
                csv = false;
            }else{
                csv = false;
                txt = false;
            }
        }catch(ArrayIndexOutOfBoundsException aiobe){
            csv = false;
            txt = false;
        }
    }

    public char[][] readFile() throws IOException{
        if(txt){
            File file = new File(fileName); 
            BufferedReader br;
            try{
                br = new BufferedReader(new FileReader(file)); 
            }catch(FileNotFoundException fnfe){
                char nullImage[][] = new char[1][1];
                return nullImage;
            }
            List<String> strList = new ArrayList<String>();
            String str; 
            while ((str = br.readLine()) != null){
                strList.add(str);
                System.out.println(str);
            }  
            br.close();       

            int maxIndex = strList.size();
            char image3[][] = new char[maxIndex][maxIndex];
            for(int i = 0; i < maxIndex; i++){
                image3[i] = (strList.get(i)).toCharArray();
            }
            return image3;  

        }else if(csv){
            File file = new File(fileName); 
            BufferedReader br;
            try{
                br = new BufferedReader(new FileReader(file)); 
            }catch(FileNotFoundException fnfe){
                char nullImage[][] = new char[1][1];
                return nullImage;
            }
            List<Integer> strListX = new ArrayList<Integer>();
            List<Integer> strListY = new ArrayList<Integer>();
            String str; 
            String point[];
            while ((str = br.readLine()) != null){
                point = str.split(",");
                strListX.add(Integer.parseInt(point[0]));
                strListY.add(Integer.parseInt(point[1]));
                System.out.println(str);
            }  
            br.close();      

            char image3[][] = fillImage(strListX, strListY);

            for(int i = 0; i < image3.length; i++){
                for(int j = 0; j < image3.length; j++){
                    System.out.print(image3[i][j]);
                }
                System.out.println();
            }
            return image3;
        }else{
            char nullImage[][] = new char[1][1];
            nullImage[0][0] = 'N';
            return nullImage;
        }
    }
    
    private static char[][] fillImage(List<Integer> listX, List<Integer> listY){
        int xIndex = Collections.max(listX);
        int yIndex = Collections.max(listY);
        int maxIndex = 0;
        int nPow;
        if(xIndex > yIndex){
            for(nPow = 1; nPow <= 10; nPow++){
                if(xIndex < Math.pow(2, nPow)){
                    maxIndex = (int)Math.pow(2, nPow);
                    break;
                }
            }
            if(nPow > 10){
                maxIndex = 1;
            }

        }else{
            for(nPow = 1; nPow <= 10; nPow++){
                if(yIndex < Math.pow(2, nPow)){
                    maxIndex = (int)Math.pow(2, nPow);
                    break;
                }
            }
            if(nPow > 10){
                maxIndex = 1;
            }
        }

        if(maxIndex > 1){
            int maxSize = listX.size();
            char image[][] = new char[maxIndex][maxIndex];
            for(int i = 0; i<maxSize; i++){
                image[listX.get(i)][listY.get(i)] = 'T';
            }   

            for(int i = 0; i < maxIndex; i++){
                for(int j = 0; j < maxIndex; j++){
                    if(image[i][j] != 'T') image[i][j] = 'F';
                }
            }

            return image;
        }else{
            char nullImage[][] = new char[1][1];
            nullImage[0][0] = 'N';
            return nullImage;
        }
    }
}
