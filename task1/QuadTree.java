import java.io.*;

public class QuadTree {
    TreeNode root;
    public TreeNode initialise(char image[][]){
        if(image.length == 1){
            TreeNode nullNode = new TreeNode();
            System.out.println("Invalid input!");
            return nullNode;
        }else{
            root = buildTree(image, 0, image.length - 1, 0, image.length - 1);
            return root;
        }
    }

    private TreeNode buildTree(char image[][], int minX, int maxX, int minY, int maxY){
        TreeNode node = new TreeNode();
        
        if(checkSame(image, minX, maxX, minY, maxY)){
            node.setVal(node.convert(image[minX][minY]));
            return node;
        }else{
            int midX = minX + (maxX - minX) / 2;
            int midY =  minY + (maxY - minY) / 2;
            node.leftLeft = buildTree(image, minX, midX, minY, midY);
            node.left = buildTree(image, minX, midX, midY + 1, maxY);
            node.right = buildTree(image, midX + 1, maxX, midY + 1, maxY);
            node.rightRight = buildTree(image, midX + 1, maxX, minY, midY);
        }

        return node;
    }

    private static boolean checkSame(char image[][], int minX, int maxX, int minY, int maxY){
        char initial;
        initial = image[minX][minY];
        for(int i = minX; i <= maxX; i++){
            for(int j = minY; j <= maxY; j++){
                if(initial != image[i][j]) return false;
            }
        }
        return true;
    }

    // Since printing the tree was not a requirement, this was referenced from: https://www.baeldung.com/java-print-binary-tree-diagram
    //  in order to view the tree properly.
    public void printTree(PrintStream os) {
        StringBuilder sb = new StringBuilder();
        traversePreOrder(sb, "", "", root);
        os.print(sb.toString());
    }

    // Since printing the tree was not a requirement, this was referenced from: https://www.baeldung.com/java-print-binary-tree-diagram
    //  in order to view the tree properly.
    public void traversePreOrder(StringBuilder sb, String padding, String edge, TreeNode node) {
        if (node != null) {
            sb.append(padding);
            sb.append(edge);
            if(node.value.valInter != -1)
                sb.append("Inter");
            else if(node.value.value) sb.append("T");
            else sb.append("F");
            sb.append("\n");
    
            StringBuilder paddingBuilder = new StringBuilder(padding);
            paddingBuilder.append("│  ");
    
            String paddingForAll = paddingBuilder.toString();
            String edgeForRightRight = "└──";
            String edgeForLeftLeft = "├──";
            String edgeForLeft = "├──";
            String edgeForRight = "├──";
    
            traversePreOrder(sb, paddingForAll, edgeForLeftLeft, node.leftLeft);
            traversePreOrder(sb, paddingForAll, edgeForLeft, node.left);
            traversePreOrder(sb, paddingForAll, edgeForRight, node.right);
            traversePreOrder(sb, paddingForAll, edgeForRightRight, node.rightRight);
        }
    }
}

