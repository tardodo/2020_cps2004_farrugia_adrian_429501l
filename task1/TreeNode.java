public class TreeNode{
    NodeStates value;
    TreeNode leftLeft;
    TreeNode left;
    TreeNode right;
    TreeNode rightRight;

    public TreeNode(){
        this.value = new NodeStates('I');
        leftLeft = null;
        left = null;
        right = null;
        rightRight = null;
    }

    public TreeNode(char value){
        //this.value = value;
        this.value = convert(value);
        leftLeft = null;
        left = null;
        right = null;
        rightRight = null;
    }

    public void setLeft(TreeNode left){
        this.left= left;
    }
    public void setRight(TreeNode right){
        this.right = right;
    }
    public void setLeftLeft(TreeNode leftLeft){
        this.leftLeft= leftLeft;
    }
    public void setRightRight(TreeNode rightRight){
        this.rightRight = rightRight;
    }
    public void setVal(NodeStates value){
        this.value = value;
    }

    public TreeNode getLeft(){
        return left;
    }
    public TreeNode getRight() {
        return right;
    }
    public TreeNode getLeftLeft(){
        return leftLeft;
    }
    public TreeNode getRightRight() {
        return rightRight;
    }
    public NodeStates getVal() {
        return value;
    }

    public NodeStates convert(char val){
        NodeStates state;
        if(val == 'T'){
            state = new NodeStates(val);
            return state;
        }else if(val == 'F'){
            state = new NodeStates(val);
            return state;
        }else{
            state = new NodeStates('I');
            return state;
        }
    }

}