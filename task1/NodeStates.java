public class NodeStates {
    boolean value;
    int valInter = -1;

    public NodeStates(char val){
        if(val == 'T') value = true;
        else if(val == 'F') value = false;
        else valInter = 0;
    }
}
