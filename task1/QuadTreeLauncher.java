import java.io.*;

public class QuadTreeLauncher {
    public static void main(String args[]) throws IOException{
        QuadTree tree = new QuadTree();
        FileHandling fl = new FileHandling(args[0]);
        char image[][] = fl.readFile();
        
        tree.initialise(image);
        System.out.println();
        tree.printTree(System.out);
        System.out.println();
        System.out.println();
        
    }
}



