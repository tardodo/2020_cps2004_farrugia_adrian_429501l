#!/bin/bash

#Test with invalid file
java QuadTreeLauncher wow.csv
#Test with txt file
java QuadTreeLauncher test.txt
#Test with csv file
java QuadTreeLauncher test.csv

exit 0