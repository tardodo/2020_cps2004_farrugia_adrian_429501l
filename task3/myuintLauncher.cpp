using namespace std; 
#include <iostream>
#include "myuint.h"

int main(){
    // No tests failed. Outputs are all as expected.
    
    myuint<32> num1(10);
    myuint<64> num2;
    myuint<32> num3;
    myuint<32> num4(5);
    myuint<32> num5(6);
    myuint<1024> num6(14500450534579367);
    myuint<2048> num7(324859935358282985);
    
    // Value assignment
    num2 = num1;

    cout << num1.bs << endl;
    cout << num2.bs << endl;
    cout << num3.bs << endl;

    // Print out large sized myuints
    cout << "\n----------Printing Large Sizes----------\n" << endl;
    cout << (num6.bs) << "\n"<< endl;
    cout << num7.bs << endl;

    // Testing comparison operators
    // Output: True and False alternating
    cout << "\n----------Comparison Operators----------\n" << endl;
    cout << (num1 == num2) << endl;
    cout << (num1 == num4) << endl;

    cout << (num1 >= num4) << endl;
    cout << (num4 >= num1) << endl;

    cout << (num4 <= num5) << endl;
    cout << (num1 <= num5) << endl;

    cout << (num1 != num4) << endl;
    cout << (num1 != num2) << endl;

    // bitwise operators
    cout << "\n----------Bitwise Operators----------\n" << endl;
    num3 = (num1 << 20);
    cout << num3.bs << endl;
    cout << (num1 & num5).bs << endl;
    cout << (num1 | num5).bs << endl;
    cout << (num1 ^ num5).bs << endl;
    cout << (~num1).bs << endl;

    // Testing arithmetic operators
    cout << "\n----------Aithmetic Operators----------\n" << endl;
    num2 = (num1*num4);
    cout << num2.bs << endl;
    cout << num2.toInt() << endl;

    num2 = (num1/num4);
    cout << num2.bs << endl;
    cout << num2.toInt() << endl;

    num2 = (num1+num4);
    cout << num2.bs << endl;
    cout << num2.toInt() << endl;

    num2 = (num1%num5);
    cout << num2.bs << endl;
    cout << num2.toInt() << endl;

    // Compoud assignment operators
    cout << "\n----------Compoud Assignment Operators----------\n" << endl;
    num1 += num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 -= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 *= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 /= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 %= num5;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 &= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 |= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 ^= num4;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 <<= 19;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    num1 >>= 6;
    cout << num1.bs << endl;
    cout << num1.toInt() << endl;

    return 0;
}