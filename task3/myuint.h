#include <bits/stdc++.h> 

using namespace std;  

template <unsigned int size>
class myuint{

    private:

        // Referenced from https://www.geeksforgeeks.org/program-to-find-whether-a-no-is-power-of-two/
        bool isPowerOfTwo(int num){
            if(num==0) return false;
            return (ceil(log2(num)) == floor(log2(num)));
        }

    public:

        bitset<size> bs;

        myuint(){
            if(!isPowerOfTwo(size)) throw this;
        }

        myuint(unsigned long long int num){
            if(isPowerOfTwo(size)){
                bitset<size> newBS(num);
                bs = newBS;
            }else{  
                throw this;
            }
        }

        template <unsigned int diffSize>
        void operator=(myuint<diffSize> obj){
            bitset<size> newBS(obj.bs.to_ullong());
            bs = newBS;
        }

        template <unsigned int diffSize>
        bool operator==(myuint<diffSize> &obj){
            if(bs.to_ullong() == obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        template <unsigned int diffSize>
        bool operator>(myuint<diffSize> &obj){
            if(bs.to_ullong() > obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        template <unsigned int diffSize>
        bool operator<(myuint<diffSize> &obj){
            if(bs.to_ullong() < obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        template <unsigned int diffSize>
        bool operator>=(myuint<diffSize> &obj){
            if(bs.to_ullong() >= obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        template <unsigned int diffSize>
        bool operator<=(myuint<diffSize> &obj){
            if(bs.to_ullong() <= obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        template <unsigned int diffSize>
        bool operator!=(myuint<diffSize> &obj){
            if(bs.to_ullong() != obj.bs.to_ullong()){
                return true;
            }
            return false;
        }

        
        myuint<size> operator&(myuint<size> &obj){
            myuint<size> newBS;
            newBS.bs = bs & obj.bs;
            return newBS;
        }

        myuint<size> operator|(myuint<size> &obj){
            myuint<size> newBS;
            newBS.bs = bs | obj.bs;
            return newBS;
        }
        myuint<size> operator^(myuint<size> &obj){
            myuint<size> newBS;
            newBS.bs = bs ^ obj.bs;
            return newBS;
        }

        myuint<size> operator~(){
            myuint<size> newBS;
            newBS.bs = ~bs;
            return newBS;
        }

        myuint<size> operator<<(int shift){
            myuint<size> newBS(bs.to_ullong());
            newBS.bs <<= shift;
            return newBS;
            //bs <<= shift;
        }

        myuint<size> operator>>(int shift){
            myuint<size> newBS(bs.to_ullong());
            newBS.bs >>= shift;
            return newBS;
            //bs <<= shift;
        }

        myuint<size> operator+(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() + obj.bs.to_ullong());
            return newBS;
        }

        myuint<size> operator-(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() - obj.bs.to_ullong());
            return newBS;
        }

        myuint<size> operator*(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong()*obj.bs.to_ullong());
            return newBS;
        }

        myuint<size> operator/(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong()/obj.bs.to_ullong());
            return newBS;
        }

        myuint<size> operator%(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong()%obj.bs.to_ullong());
            return newBS;
        }

        void operator+=(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() + obj.bs.to_ullong());
            bs = newBS.bs;
        }

        void operator-=(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() - obj.bs.to_ullong());
            bs = newBS.bs;
        }

        void operator*=(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() * obj.bs.to_ullong());
            bs = newBS.bs;
        }

        void operator/=(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() / obj.bs.to_ullong());
            bs = newBS.bs;
        }

        void operator%=(myuint<size> &obj){
            myuint<size> newBS(bs.to_ullong() % obj.bs.to_ullong());
            bs = newBS.bs;
        }

        void operator&=(myuint<size> &obj){
            bs = bs & obj.bs;
        }

        void operator^=(myuint<size> &obj){
            bs = bs & obj.bs;
        }

        void operator|=(myuint<size> &obj){
            bs = bs & obj.bs;
        }

        void operator>>=(int shift){
            bs = bs >> shift;
        }

        void operator<<=(int shift){
            bs = bs << shift;
        }

        unsigned long long toInt(){
            return bs.to_ullong();
        }

        

        
};